<?php

	include 'inc.twig.php'; // il manquait le e de include
	
	$template_index = $twig->loadTemplate('index.tpl'); // il manquait le ;

	$n_jours_previsions = 3;

	$ville = "Limoges"; 

	//~ Clé API
	//~ Si besoin, vous pouvez générer votre propre clé d'API gratuitement, en créant 
	//~ votre propre compte ici : https://home.openweathermap.org/users/sign_up

	$apikey = "10eb2d60d4f267c79acb4814e95bc7dc"; 

	$data_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?APPID='.$apikey.'&q='.$ville.',fr&lang=fr&units=metric&cnt='.$n_jours_previsions;
	
	$data_contenu = file_get_contents($data_url);
		
	$_data_array = json_decode($data_contenu, true);

	$_ville = $_data_array['city']['name']; // $_data_array['city'] est un array, il faut spécifier ['name'] pour accéder au nom de la ville.
	$_journees_meteo = $_data_array['list'];

	$_ville_country = $_data_array['city']['country']; //On a ajouté cette variable pour pouvoir l'appeler sur index.tpl et compléter le titre

	// var_dump($_data_array);
	// Grâce à l'utilisation de var_dump, j'ai lu $_data_array et j'ai pu voir que c'était 'list' et non 'liste'

	for ($i = 0; $i < count($_journees_meteo); $i++) {
		$_meteo = getMeteoImage($_journees_meteo[$i]['weather'][0]['icon']);
		
		$_journees_meteo[$i]['meteo'] = $_meteo;
	}

	echo $template_index->render(array(
		'_journees_meteo'	=> $_journees_meteo,
		'_ville'			=> $_ville, // il manquait le > après le =
		'n_jours_previsions'=> $n_jours_previsions,
		'_ville_country' => $_ville_country //on ajoute cette variable à cet array pour qu'elle soit prise en compte sur index.tpl
	));

	function getMeteoImage($code){
		if(strpos($code, 'n')){ // ouvrir et fermer les acolates
			return 'entypo-moon';
		}

		// indentation de l'array ci-dessous 
		$_icones_meteo = array(
			'01d' => 'entypo-light-up',
			'02d' => 'entypo-light-up',
			'03d' => 'entypo-cloud', // il manquait la virgule
			'04d' => 'entypo-cloud',
			'09d' => 'entypo-water', 
			'10d' => 'entypo-water',
			'11d' => 'entypo-flash',
			'13d' => 'entypo-star', 
			'50d' => 'entypo-air', // rajout d'une virgule
		);

		if(array_key_exists($code, $_icones_meteo)){ // il manquait l'accolade ouvrante
			return $_icones_meteo[$code];
		}else{
			return 'entypo-help';
		}
	}
// Il manquait le signe de fermeture de php
?> 
